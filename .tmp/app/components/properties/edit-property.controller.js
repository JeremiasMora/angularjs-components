angular
	.module('app')
	.controller('editPropertyCtrl', editPropertyCtrl);

function editPropertyCtrl($scope, propertyService, $state, $stateParams) {

	$scope.propertyForm = propertyService.get({id: $stateParams.id});

	$scope.editProperty = function (property) {
		propertyService.save($scope.propertyForm, function (success) {
			$state.go('property', {}, {reload: true});
		}, function (err) {
			console.log(err);
		});
	};
}
