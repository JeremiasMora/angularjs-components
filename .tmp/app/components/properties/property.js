angular
  .module('app')
  .component('property', {
    templateUrl: 'app/components/properties/property.html',
    controller: propertyCtrl
  });

function propertyCtrl($scope, propertyService, $log) {
	$scope.properties = propertyService.query();
	console.log($scope.properties); 

	$scope.removeProperty = function(property){
		console.log(property);
		propertyService.delete({id: property.id}, function(data){
			console.log(data);
			var index = $scope.properties.indexOf(data);
			$scope.properties.splice(index, 1);
		});
	}
}
