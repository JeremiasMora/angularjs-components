angular
.module('app')
.service('propertyService', propertyService);

function propertyService($resource) {
	return $resource('http://jero.local/api/properties/:id', {id: '@_id'},
		{
		 	update: {
		 		method: 'PUT'
		 	}
		}
	);
}
