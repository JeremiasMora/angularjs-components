angular
	.module('app')
	.controller('createUserCtrl', createUserCtrl);

function createUserCtrl($scope, userService, $state) {

	$scope.properties = userService.resourceProperty().query();
	
	$scope.userForm = {
		name: '',
		email: '',
		password: '',
		properties_id: ''
	};

	$scope.saveUser = function (user) {
		user.properties_id = user.properties_id.id;
		console.log(user);
		userService.resource().save(user, function (success) {
			$state.go('app', {}, {reload: true});
		}, function (err) {
			console.log(err);
		});
	};
}
