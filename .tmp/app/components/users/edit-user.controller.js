angular
	.module('app')
	.controller('editUserCtrl', editUserCtrl);

function editUserCtrl($scope, userService, $state, $stateParams) {
	
	$scope.userForm = userService.resource().get({id: $stateParams.id});
	$scope.properties = userService.resourceProperty().query();


	$scope.saveUser = function (user) {
		user.properties_id = user.properties_id.id;
		console.log(user);
		userService.resource().update({id: user.id}, user, function (success) {
			$state.go('app', {}, {reload: true});
		}, function (err) {
			console.log(err);
		});
	};
}
