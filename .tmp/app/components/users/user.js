angular
  .module('app')
  .component('user', {
    templateUrl: 'app/components/users/user.html',
    controller: userCtrl
  });

function userCtrl($scope, userService) {
	$scope.users = userService.resource().query();
	 
	$scope.removeUser = function(user){
		console.log(user);
		userService.resource().delete({id: user.id}, function(data){
			console.log(data);
			var index = $scope.users.indexOf(data);
			$scope.users.splice(index, 1);
		});
	}
}
