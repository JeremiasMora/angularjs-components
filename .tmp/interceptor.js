angular
	.module('app')
	.service('Interceptor', interceptorService);

function interceptorService($rootScope) {
  var service = this;
  service.request = function (config) {
    var accessToken = 'base64:YLwxkQAx6rbru+EHtqQ7S/CsMl0kjRtxtpp5DvTZXI0=';
    config.headers.authorization = accessToken;

    return config;
  };
  service.responseError = function (response) {
    if (response.status === 401) {
      $rootScope.$broadcast('unauthorized');
    }
    return response;
  };
}
