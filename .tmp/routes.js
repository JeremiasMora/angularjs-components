angular
  .module('app')
  .config(routesConfig);

/** @ngInject */
function routesConfig($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {
  $httpProvider.interceptors.push('Interceptor');
  $locationProvider.html5Mode(true).hashPrefix('!');
  $urlRouterProvider.otherwise('/');

  $stateProvider
    .state('app', {
      url: '/',
      component: 'user'
    })
    .state('createUser', {
      url: '/createUser',
      templateUrl: '/app/components/users/new-user.html',
      controller: 'createUserCtrl'
    })
    .state('editUser', {
      url: '/editUser/:id',
      templateUrl: '/app/components/users/new-user.html',
      controller: 'editUserCtrl'
    })
    .state('viewUser', {
      url: '/viewUser/:id',
      templateUrl: '/app/components/users/view-user.html',
      controller: 'viewUserCtrl'
    })
    .state('property', {
      url: '/properties',
      component: 'property'
    })
    .state('createProperty', {
      url: '/createProperty',
      templateUrl: '/app/components/properties/new-property.html',
      controller: 'createPropertyCtrl'
    })
    .state('editProperty', {
      url: '/editProperty/:id',
      templateUrl: '/app/components/properties/new-property.html',
      controller: 'editPropertyCtrl'
    })
    .state('viewProperty', {
      url: '/viewProperty/:id',
      templateUrl: '/app/components/properties/view-property.html',
      controller: 'viewPropertyCtrl'
    });
}
