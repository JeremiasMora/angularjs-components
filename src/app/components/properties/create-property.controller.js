angular
	.module('app')
	.controller('createPropertyCtrl', createPropertyCtrl);

function createPropertyCtrl($scope, propertyService, $state) {

	$scope.propertyForm = {
		name: '',
		description: ''
	};

	$scope.saveProperty = function (property) {
		propertyService.save($scope.propertyForm, function (success) {
			$state.go('property', {}, {reload: true});
		}, function (err) {
			console.log(err);
		});
	};
}
