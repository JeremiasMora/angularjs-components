angular
.module('app')
.service('userService', userService);

function userService($http, $resource) {
	var service = this;
	service.resource = function () {
		return $resource('http://jero.local/api/users/:id', {id: '@_id'},
			{
			 	update: {
			 		method: 'PUT'
			 	}
			}
		);
	}
	service.resourceProperty = function () {
		return $resource('http://jero.local/api/properties/:id', {id: '@_id'},
			{
			 	update: {
			 		method: 'PUT'
			 	}
			}
		);
	};
}
